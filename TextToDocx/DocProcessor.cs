﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace TextToDocx
{
    /// <summary>
    /// Lots of syntactic sugar... makes for some hard to read code but.. its pretty! Maybe I should
    /// have just used Rust and got the performance from it also.
    /// </summary>
    class DocProcessor
    {
        IDocumentWriter writer;
        int depth;
        Dictionary<string, string> footnotes = new Dictionary<string, string>();
        Func<string, IEnumeratorStyleChoice> convert_string_to_style_choice;


        public DocProcessor(
            IDocumentWriter writer,
            Func<string, IEnumeratorStyleChoice> convert_string_to_style_choice
            )
        {
            this.convert_string_to_style_choice = convert_string_to_style_choice;
            this.writer = writer;
            this.depth = 0;
        }

        public void DepthUpDown(Action action)
        {
            depth++;
            action();
            depth--;
        }

        public static void PrettyForEach(
            IEnumerable<IEnumeratorHelper> enumer, 
            Dictionary<string, Action<IEnumeratorHelper>> cases)
        {
            foreach (var item in enumer)
            {
                var tname = item.GetTagName();

                if (cases.ContainsKey(tname))
                {
                    cases[tname](item);
                } else
                {
                    throw new ParseException("The tag is not supported.", item.GetLineAndPosition());
                }
            }
        }

        public void DocumentRoot(IEnumeratorStyleChoice enumer_choice)
        {
            PrettyForEach(
                enumer_choice.GetContentsAsEnumerable(),
                new Dictionary<string, Action<IEnumeratorHelper>> {
                    { "s", item => {
                        var title = item.GetAttribute("title");

                        DepthUpDown(() => Section(item.GiveCalledChoiceOfStyle(), title));
                    }},
                    { "d", item => {
                        // Ignore it.
                        // This is actually what we are supposed to handle and in the future
                        // a lower level handler may be beneficial to implement. This would
                        // reduce this class to just handling "s" for sections and any other
                        // future additions - instead of whats outside the <d> element.
                    }},
                    { "xml", item => {
                        // Ignore it.
                    }},
                    { "$text", item => {
                        // Ignore it.
                    }},
                    { "footnote", item => {
                        var key = item.GetAttribute("key");
                        var val = item.GetContentsAsString();

                        footnotes.Add(key, val);
                    }},
                    { "keyword-auto-cite", item => {
                        var enabled = bool.Parse(item.GetAttribute("enabled"));
                        var prefix = item.GetAttribute("prefix");

                        feature_keyword_auto_cite = enabled;
                        feature_keyword_auto_cite_prefix = prefix;
                    }},
                }
            );
        }

        public IEnumerable<IEnumeratorHelper> PesudoDocumentRoot(IEnumeratorStyleChoice enumer_choice)
        {
            foreach (var item in enumer_choice.GetContentsAsEnumerable())
            {
                var tname = item.GetTagName();

                if (tname.Equals("pd"))
                {
                    continue;
                }

                yield return item;
            }
        }

        public void Section(IEnumeratorStyleChoice enumer_choice, string title)
        {
            writer.AddSection(depth, title);

            PrettyForEach(
                enumer_choice.GetContentsAsEnumerable(),
                new Dictionary<string, Action<IEnumeratorHelper>> {
                    { "p", item => {
                        Paragraph(item.GiveCalledChoiceOfStyle());
                    }},
                    { "footnote", item => {
                        var key = item.GetAttribute("key");
                        var val = item.GetContentsAsString();

                        footnotes.Add(key, val);
                    }},
                    { "s", item => {
                        var new_title = item.GetAttribute("title");

                        DepthUpDown(() => Section(item.GiveCalledChoiceOfStyle(), new_title));
                    }},
                }
            );
        }

        public void Paragraph(IEnumeratorStyleChoice enumer_choice, bool do_not_emit_paragraph = false)
        {
            if (!do_not_emit_paragraph)
                writer.AddParagraph(depth);

            PrettyForEach(
                enumer_choice.GetContentsAsEnumerable(),
                new Dictionary<string, Action<IEnumeratorHelper>> {
                    { "$text", item => {
                        writer.AddRunAndText(item.GetContentsAsString());
                    }},
                    { "name", item => {
                        Name(item.GiveCalledChoiceOfStyle());
                    }},
                    { "phone", item => {
                        Phone(item.GiveCalledChoiceOfStyle());
                    }},
                    { "email", item => {
                        Email(item.GiveCalledChoiceOfStyle());
                    }},
                    { "keyword", item => {
                        Keyword(item.GiveCalledChoiceOfStyle());
                    }},
                    { "c", item => {
                        Cite(item.GiveCalledChoiceOfStyle());
                    }},
                    { "string", item => {
                        Strong(item.GiveCalledChoiceOfStyle());
                    }},
                    { "url", item => {
                        Url(item.GiveCalledChoiceOfStyle());
                    }},
                    { "title", item => {
                        Title(item.GiveCalledChoiceOfStyle());
                    }},
                }
            );

            if (!do_not_emit_paragraph)
                writer.GotoParent();
        }

        public void Url(IEnumeratorStyleChoice enumer_choice)
        {
            writer.AddRunAndText(enumer_choice.GetContentsAsString(), StyleCreaterType.Url);
        }

        public void Title(IEnumeratorStyleChoice enumer_choice)
        {
            writer.AddRunAndText(enumer_choice.GetContentsAsString(), StyleCreaterType.LongWork);
        }

        public void Strong(IEnumeratorStyleChoice enumer_choice)
        {
            writer.AddRunAndText(enumer_choice.GetContentsAsString(), StyleCreaterType.Strong);
        }


        Dictionary<string, int> footnote_to_refid = new Dictionary<string, int>();

        int citation_generator(string text, bool is_dup, int refid)
        {
            text = text.Trim();

            if (footnote_to_refid.ContainsKey(text))
            {
                refid = footnote_to_refid[text];
            }
            else
            {
                footnote_to_refid.Add(text, refid);
            }

            var choice_object = convert_string_to_style_choice(text);

            Paragraph(
                new EnumeratorStyleChoice(PesudoDocumentRoot(choice_object)),
                // For compatibility reasons, the writer already emits a needed 
                // paragraph. Emitting two will really make the spacings messed
                // up in the footnote area.
                do_not_emit_paragraph: true
            );

            return refid;
        }

        public void Cite(IEnumeratorStyleChoice enumer_choice)
        {
            var key = enumer_choice.GetContentsAsString();

            if (footnotes.ContainsKey(key))
            {
                writer.StartFootnoteCitation(
                    citation_generator,
                    footnotes[key]
                );
            }
            else
            {
                throw new Exception($"Missing citation data for key {key}.");
            }
        }

        bool feature_keyword_auto_cite = false;
        string feature_keyword_auto_cite_prefix = "";

        public void Keyword(IEnumeratorStyleChoice enumer_choice)
        {
            var val = enumer_choice.GetContentsAsString();

            writer.AddRunAndText(val, StyleCreaterType.Keyword);

            if (feature_keyword_auto_cite)
            {
                var key = $"{feature_keyword_auto_cite_prefix}_{val}".ToLower();

                if (footnotes.ContainsKey(key))
                {
                    writer.StartFootnoteCitation(
                        citation_generator,
                        footnotes[key]
                    );
                }
            }
        }

        public void Email(IEnumeratorStyleChoice enumer_choice)
        {
            writer.AddRunAndText(enumer_choice.GetContentsAsString(), StyleCreaterType.Email);
        }

        public void Name(IEnumeratorStyleChoice enumer_choice)
        {
            writer.AddRunAndText(enumer_choice.GetContentsAsString(), StyleCreaterType.Name);
        }

        public void Phone(IEnumeratorStyleChoice enumer_choice)
        {
            writer.AddRunAndText(enumer_choice.GetContentsAsString(), StyleCreaterType.Phone);
        }
    }
}