﻿using System;
using System.Collections.Generic;

namespace TextToDocx
{
    interface IEnumeratorStyleChoice: IDisposable
    {
        IEnumerable<IEnumeratorHelper> GetContentsAsEnumerable();
        string GetContentsAsString();
    }
}