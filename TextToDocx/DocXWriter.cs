﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using System;
using System.Collections.Generic;

namespace TextToDocx
{
    class DocXWriter: IDisposable, IDocumentWriter
    {
        Dictionary<int, string> style_creator_type_name_map = new Dictionary<int, string>();

        WordprocessingDocument pkg;
        string name;
        FootnotesPart footnotes_part;
        OpenXmlElement current_element;
        StyleDefinitionsPart styles_part;
        Queue<OpenXmlElement> current_element_stack = new Queue<OpenXmlElement>();

        Footnotes footnotes = new Footnotes();
        Body body = new Body();
        Dictionary<string, Style> styles = new Dictionary<string, Style>();
        Document doc = new Document();
        bool text_added_to_paragraph = false;
        int footnote_unique_id = 100;
        Action<Style, StyleCreaterType, int> style_callback = null;
        Dictionary<string, int> footnote_to_ids = new Dictionary<string, int>();

        public DocXWriter(string name, Action<Style, StyleCreaterType, int> style_callback)
        {
            this.style_callback = style_callback;
            this.name = name;

            style_creator_type_name_map.Add((int)StyleCreaterType.Email, "Email");
            style_creator_type_name_map.Add((int)StyleCreaterType.LongWork, "LongWork");
            style_creator_type_name_map.Add((int)StyleCreaterType.Name, "Name");
            style_creator_type_name_map.Add((int)StyleCreaterType.ParagraphText, "ParagraphText");
            style_creator_type_name_map.Add((int)StyleCreaterType.Phone, "Phone");
            style_creator_type_name_map.Add((int)StyleCreaterType.SectionTitle, "SectionTitle");
            style_creator_type_name_map.Add((int)StyleCreaterType.ShortWork, "ShortWork");
            style_creator_type_name_map.Add((int)StyleCreaterType.Strong, "Strong");
            style_creator_type_name_map.Add((int)StyleCreaterType.Keyword, "Keyword");
            style_creator_type_name_map.Add((int)StyleCreaterType.FootnoteReference, "FootnoteReference");
            style_creator_type_name_map.Add((int)StyleCreaterType.Url, "Url");

            current_element = body;

            pkg = WordprocessingDocument.Create(name, DocumentFormat.OpenXml.WordprocessingDocumentType.Document);

            pkg.AddMainDocumentPart();

            footnotes_part = pkg.MainDocumentPart.AddNewPart<FootnotesPart>();
            footnotes_part.Footnotes = footnotes;

            styles_part = pkg.MainDocumentPart.AddNewPart<StyleDefinitionsPart>();
            styles_part.Styles = new Styles();

            doc.AppendChild(body);

            pkg.MainDocumentPart.Document = doc;
        }

        public void AddHyperlink(string v, StyleCreaterType style_type = StyleCreaterType.ParagraphText, int level = 0)
        {
            v = v.Replace("\t", " ");
            v = v.Replace("\n", " ");

            while (v.IndexOf("  ") > -1)
            {
                v = v.Replace("  ", " ");
            }

            v = v.Trim();

            if (text_added_to_paragraph)
            {
                var c = v[0];

                if (
                    c != '.' &&
                    c != '!' &&
                    c != '?'
                )
                    v = $" {v}";
            }
            else
            {
                text_added_to_paragraph = true;
            }

            var run = new Run(new Text(v)
            {
                Space = SpaceProcessingModeValues.Preserve,
            });

            var id = ConsiderStyleNeed(style_type, level);

            run.RunProperties = new RunProperties()
            {
                RunStyle = new RunStyle()
                {
                    Val = id,
                }
            };

            current_element.AppendChild(run);
        }

        public void AddRunAndText(string v, StyleCreaterType style_type = StyleCreaterType.ParagraphText, int level = 0)
        {
            v = v.Trim();

            v = v.Replace("\t", " ");
            v = v.Replace("\n", " ");

            while (v.IndexOf("  ") > -1)
            {
               v = v.Replace("  ", " ");
            }

            if (text_added_to_paragraph)
            {
                var c = v[0];

                if (
                    c != '.' &&
                    c != '!' &&
                    c != '?'
                )
                    v = $" {v}";
            } else
            {
                text_added_to_paragraph = true;
            }

            var run = new Run(new Text(v)
            {
                Space = SpaceProcessingModeValues.Preserve,
            });

            var id = ConsiderStyleNeed(style_type, level);

            run.RunProperties = new RunProperties()
            {
                RunStyle = new RunStyle()
                {
                    Val = id,
                }
            };

            current_element.AppendChild(run);
        }

        private string ConsiderStyleNeed(StyleCreaterType type, int level)
        {
            string id;
            string name;

            if (style_creator_type_name_map.ContainsKey((int)type)) {
                id = $"{style_creator_type_name_map[(int)type]}{level}";
                name = $"{style_creator_type_name_map[(int)type]}{level}";
            } else
            {
                id = $"unknown{level}";
                name = $"unknown{level}";
            }

            if (!styles.ContainsKey(name))
            {
                var style = new Style()
                {
                    StyleId = id,
                    StyleName = new StyleName() { Val = name },
                    Type =  type == StyleCreaterType.ParagraphText || 
                            type == StyleCreaterType.SectionTitle ? 
                            StyleValues.Paragraph : 
                            StyleValues.Character,
                    PrimaryStyle = new PrimaryStyle(),
                };

                style_callback(
                    style,
                    type,
                    level
                );

                styles.Add(name, style);
                styles_part.RootElement.Append(style);
            }

            return id;
        }

        public void AddSection(int level, string title)
        {
            var p = new Paragraph();

            p.AppendChild(new Run(new Text(title)));

            var style_id = ConsiderStyleNeed(StyleCreaterType.SectionTitle, level);

            var props = new ParagraphProperties(
                new ParagraphStyleId()
                {
                    Val = style_id,
                }
            );

            p.ParagraphProperties = props;

            current_element.AppendChild(p);
        } 

        public void AddParagraph(int level = 0)
        {
            var p = new Paragraph();

            var style_id = ConsiderStyleNeed(StyleCreaterType.ParagraphText, level);

            var props = new ParagraphProperties(
                new ParagraphStyleId()
                {
                    Val = style_id,
                }
            );

            p.ParagraphProperties = props;

            current_element.AppendChild(p);

            current_element = p;

            text_added_to_paragraph = false;
        }

        public void GotoParent()
        {
            current_element = current_element.Parent;
        }

        public void PushCurrentElement()
        {
            current_element_stack.Enqueue(current_element);
        }

        public void PopCurrentElement()
        {
            current_element = current_element_stack.Dequeue();
        }

        public void SetCurrentElement(OpenXmlElement current_element)
        {
            this.current_element = current_element;
        }

        public void StartFootnoteCitation(
            Func<string, bool, int, int> builder, 
            string text, 
            int level = 0, 
            bool only_output_definition = false)
        {
            var is_duplicate = footnote_to_ids.ContainsKey(text);

            footnote_unique_id += 1;

            var para = new Paragraph();

            PushCurrentElement();

            current_element = para;

            var refid = builder(text, is_duplicate, footnote_unique_id);

            PopCurrentElement();

            var fn = new Footnote();

            var _r = new Run();

            _r.AppendChild(new Text(". ")
            {
                Space = SpaceProcessingModeValues.Preserve,
            });

            para.PrependChild(_r);

            _r = new Run();

            if (only_output_definition)
            {
                _r.AppendChild(new FootnoteReference()
                {
                    Id = refid,
                });
            }
            else
            {
                _r.AppendChild(new FootnoteReferenceMark());
            }

            para.PrependChild(_r);

            if (only_output_definition)
            {
                current_element.AppendChild(para);
                return;
            }

            fn.Id = refid;

            fn.AppendChild(para);
            footnotes.AppendChild(fn);

            var fnref = new FootnoteReference()
            {
                Id = refid,
            };

            var style_id = ConsiderStyleNeed(StyleCreaterType.FootnoteReference, level);

            var r = new Run(fnref)
            {
                RunProperties = new RunProperties()
                {
                    RunStyle = new RunStyle()
                    {
                        Val = style_id,
                    },
                },
            };

            current_element.AppendChild(r);
        }

        public void Dispose()
        {
            pkg.Dispose();
        }

        public void Save() {

            pkg.MainDocumentPart.StyleDefinitionsPart.Styles.Save();
            pkg.MainDocumentPart.Document.Save();

            pkg.Dispose();
        }
    }
}