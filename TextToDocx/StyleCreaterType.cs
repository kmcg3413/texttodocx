﻿namespace TextToDocx
{
    enum StyleCreaterType
    {
        SectionTitle,
        ParagraphText,
        Email,
        Phone,
        Name,
        Strong,
        LongWork,
        ShortWork,
        Keyword,
        FootnoteReference,
        Footnote,
        Url,
    }
}