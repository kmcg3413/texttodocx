﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Wordprocessing;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using System.Xml;

namespace TextToDocx
{
    public class Denotation : Attribute
    {
        string tag;

        public Denotation(string tag)
        {
            this.tag = tag;
        }
    }

    interface IEnumeratorHelper: IDisposable
    {
        string GetTagName();
        string GetAttribute(string key);
        string GetContentsAsString();
        IEnumerable<IEnumeratorHelper> GetContentsAsEnumerable();
        IEnumeratorStyleChoice GiveCalledChoiceOfStyle();
        (int, int) GetLineAndPosition();
        bool Valid();
    }

    class ParseException : Exception
    {
        public int line { get; }
        public int pos { get; }
        public string msg { get; }

        public ParseException(string msg, (int, int) p)
        {
            this.msg = msg;
            this.line = p.Item1;
            this.pos = p.Item2;
        }
    }

    class EnumeratorHelperXml : IEnumeratorHelper
    {
        XmlReader xml;
        public bool skip_read;

        public EnumeratorHelperXml(XmlReader xml)
        {
            this.xml = xml;
            this.skip_read = false;
        }

        public bool Valid()
        {
            switch (xml.NodeType)
            {
                case XmlNodeType.Element:
                    // Workaround for having to wrap the XML string to get an enumerable of inner tags.
                    // There is likely a better way.
                    if (GetTagName() == "ignore-ignore")
                        return false;
                    return true;
                case XmlNodeType.Text:
                    return true;
            }

            return false;
        }

        public string GetTagName()
        {
            switch (xml.NodeType)
            {
                case XmlNodeType.Element:
                    return xml.Name;
                case XmlNodeType.Whitespace:
                    return "$whitespace";
                case XmlNodeType.Comment:
                    return "$comment";
                case XmlNodeType.SignificantWhitespace:
                    return "$whitespace";
                case XmlNodeType.Text:
                    return "$text";
                case XmlNodeType.XmlDeclaration:
                    return "$whitespace";
            }

            throw new NotImplementedException($"Handling of XMLNodeType {xml.NodeType} not implemented.");
        }

        public string GetAttribute(string key)
        {
            return xml.GetAttribute(key);
        }

        public string GetContentsAsString()
        {
            switch (xml.NodeType)
            {
                case XmlNodeType.Element:
                    skip_read = true;
                    return xml.ReadInnerXml();
                case XmlNodeType.Whitespace:
                    return "";
                case XmlNodeType.Comment:
                    return "";
                case XmlNodeType.SignificantWhitespace:
                    return "";
                case XmlNodeType.Text:
                    return xml.Value;
            }

            throw new NotImplementedException($"Handling of XMLNodeType {xml.NodeType} not implemented.");
        }

        public IEnumerable<IEnumeratorHelper> GetContentsAsEnumerable()
        {
            switch (xml.NodeType)
            {
                case XmlNodeType.Element:
                    skip_read = true;
                    var s = GetContentsAsString();
                    var ms = new MemoryStream(Encoding.UTF8.GetBytes($"<ignore-ignore>{s}</ignore-ignore>"));
                    return DocXmlProcessor.ShallowXmlEnumerator(XmlReader.Create(ms));
            }

            throw new NotImplementedException($"Handling of XMLNodeType {xml.NodeType} not implemented.");
        }

        public IEnumeratorStyleChoice GiveCalledChoiceOfStyle()
        {
            return new EnumeratorStyleChoice(this);
        }

        public (int, int) GetLineAndPosition()
        {
            return (0, 0);
        }

        public void Dispose()
        {
            xml = null;
        }
    }

    class EnumeratorStyleChoice : IEnumeratorStyleChoice
    {
        IEnumeratorHelper enumer = null;
        IEnumerable<IEnumeratorHelper> only_enumer = null;

        public EnumeratorStyleChoice(IEnumeratorHelper enumer)
        {
            this.enumer = enumer;
        }

        public EnumeratorStyleChoice(IEnumerable<IEnumeratorHelper> only_enumer)
        {
            this.only_enumer = only_enumer;
        }

        public string GetContentsAsString()
        {
            if (enumer == null)
            {
                throw new NotImplementedException("Need implementation to convert `only_enumer` into `enumer`. See source.");
            }

            var tmp = enumer;

            // Play it safe. Prevent future accidental access.
            enumer = null;

            return tmp.GetContentsAsString();
        }

        public IEnumerable<IEnumeratorHelper> GetContentsAsEnumerable()
        {
            if (only_enumer != null)
            {
                var tmp = only_enumer;
                // Play it safe. Prevent future accidental access.
                this.only_enumer = null;
                return tmp;
            }
            else
            {
                var tmp = enumer;
                // Play it safe. Prevent future accidental access.
                enumer = null;
                return tmp.GetContentsAsEnumerable();
            }
        }

        public void Dispose()
        {
            enumer = null;
            only_enumer = null;
        }
    }

    static class DocXmlProcessor
    {
        public static IEnumerable<IEnumeratorHelper> ShallowXmlEnumerator(XmlReader xml)
        {
            var sh = new EnumeratorHelperXml(xml);

            while (sh.skip_read || xml.Read())
            {
                sh.skip_read = false;

                if (!sh.Valid())
                {
                    // XML has a lot of various types but we only want to force
                    // the agnostic user of this (based on IEnumeratorHelper) to
                    // deal with just what it needs. See the `Valid` method to
                    // understand what is dealt with and also how.
                    continue;
                }

                yield return sh;
            }
        }
    }

    public class Program
    {
        /// <summary>
        /// Not agnostic to DocXWriter. Agnostic to DocProcessor.
        /// </summary>
        /// <param name="style"></param>
        /// <param name="type"></param>
        /// <param name="level"></param>
        static void style_creator(Style style, StyleCreaterType type, int level)
        {
            var run_props = new StyleRunProperties();

            var para_props = new StyleParagraphProperties();

            run_props.RunFonts = new RunFonts()
            {
                ComplexScriptTheme = ThemeFontValues.MajorBidi,
                HighAnsiTheme = ThemeFontValues.MajorHighAnsi,
                EastAsiaTheme = ThemeFontValues.MajorEastAsia,
                AsciiTheme = ThemeFontValues.MajorAscii,
            };

            run_props.Color = new Color()
            {
                Val = "000000",
                ThemeShade = "BL",
                ThemeColor = ThemeColorValues.None,
            };

            run_props.RunFonts = new RunFonts() { Ascii = "Lora" };
            run_props.Bold = new Bold() { Val = OnOffValue.FromBoolean(false) };
            run_props.FontSize = new FontSize() { Val = "24" };

            switch (type)
            {
                case StyleCreaterType.Url:
                    run_props.Underline = new Underline() { Val = UnderlineValues.Double };
                    break;
                case StyleCreaterType.SectionTitle:
                    run_props.Bold = new Bold() { Val = OnOffValue.FromBoolean(true) };
                    run_props.FontSize = new FontSize() { Val = (24 * 2 - level * 2).ToString() };
                    break;
                case StyleCreaterType.ParagraphText:
                    para_props.Indentation = new Indentation()
                    {
                        FirstLine = "0.5in",
                    };
                    break;
                case StyleCreaterType.FootnoteReference:
                    run_props.VerticalTextAlignment = new VerticalTextAlignment()
                    {
                        Val = VerticalPositionValues.Superscript,
                    };
                    break;
                case StyleCreaterType.Name:
                    break;
                case StyleCreaterType.Phone:
                    break;
                case StyleCreaterType.Email:
                    break;
                case StyleCreaterType.Keyword:
                    break;
                case StyleCreaterType.LongWork:
                    break;
                case StyleCreaterType.ShortWork:
                    break;
                case StyleCreaterType.Strong:
                    break;
            }


            style.StyleRunProperties = run_props;
            style.StyleParagraphProperties = para_props;
        }

        public static void Main(string[] args)
        {
            var writer = new DocXWriter("test.docx", style_creator);
            var dprocessor = new DocProcessor(writer, (text) =>
            {
                // Bridges between the agnostic DocProcessor and the underlying
                // implementation that uses XML. For example, instead of XML we
                // could use JSON or some other format. DocProcessor is only interesting
                // in the generalized structure which happens to be a good fit for XML.
                var new_text = $"<pd>{text}</pd>";

                var ms = new MemoryStream(Encoding.UTF8.GetBytes(new_text));
                var enumer = DocXmlProcessor.ShallowXmlEnumerator(XmlReader.Create(ms));
                return new EnumeratorStyleChoice(enumer);
            });

            using (var s = Assembly.GetExecutingAssembly().GetManifestResourceStream("TextToDocx.testone.xml"))
            {
                //db.Run(s, "test.docx");

                var enumer = DocXmlProcessor.ShallowXmlEnumerator(XmlReader.Create(s));

                dprocessor.DocumentRoot(new EnumeratorStyleChoice(enumer));

                writer.Save();
            }
        }
    }
}