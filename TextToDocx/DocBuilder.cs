﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Wordprocessing;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;

namespace TextToDocx
{
    public class DocBuilder
    {
        Dictionary<string, string> footnotes = new Dictionary<string, string>();
        Dictionary<string, int> footnote_to_refid = new Dictionary<string, int>();

        bool feature_keyword_auto_cite = false;
        string feature_keyword_auto_cite_prefix = "";

        public DocBuilder()
        {

        }

        void style_creator(Style style, StyleCreaterType type, int level)
        {
            var run_props = new StyleRunProperties();

            var para_props = new StyleParagraphProperties();

            run_props.RunFonts = new RunFonts()
            {
                ComplexScriptTheme = ThemeFontValues.MajorBidi,
                HighAnsiTheme = ThemeFontValues.MajorHighAnsi,
                EastAsiaTheme = ThemeFontValues.MajorEastAsia,
                AsciiTheme = ThemeFontValues.MajorAscii,
            };

            run_props.Color = new Color()
            {
                Val = "000000",
                ThemeShade = "BL",
                ThemeColor = ThemeColorValues.None,
            };

            run_props.RunFonts = new RunFonts() { Ascii = "Lora" };
            run_props.Bold = new Bold() { Val = OnOffValue.FromBoolean(false) };
            run_props.FontSize = new FontSize() { Val = "24" };

            switch (type)
            {
                case StyleCreaterType.Url:
                    run_props.Underline = new Underline() { Val = UnderlineValues.Double };
                    break;
                case StyleCreaterType.SectionTitle:
                    run_props.Bold = new Bold() { Val = OnOffValue.FromBoolean(true) };
                    run_props.FontSize = new FontSize() { Val = (24 * 2 - level * 2).ToString() };
                    break;
                case StyleCreaterType.ParagraphText:
                    para_props.Indentation = new Indentation()
                    {
                        FirstLine = "0.5in",
                    };
                    break;
                case StyleCreaterType.FootnoteReference:
                    run_props.VerticalTextAlignment = new VerticalTextAlignment()
                    {
                        Val = VerticalPositionValues.Superscript,
                    };
                    break;
                case StyleCreaterType.Name:
                    break;
                case StyleCreaterType.Phone:
                    break;
                case StyleCreaterType.Email:
                    break;
                case StyleCreaterType.Keyword:
                    break;
                case StyleCreaterType.LongWork:
                    break;
                case StyleCreaterType.ShortWork:
                    break;
                case StyleCreaterType.Strong:
                    break;
            }


            style.StyleRunProperties = run_props;
            style.StyleParagraphProperties = para_props;
        }

        int BuildFootNote(DocXWriter doc, string text, bool is_dup, int refid)
        {
            text = text.Trim();

            if (footnote_to_refid.ContainsKey(text))
            {
                refid = footnote_to_refid[text];
            } else
            {
                footnote_to_refid.Add(text, refid);
            }

            ContinueBuildingDocUsingInnerContent(doc, text);

            return refid;
        }

        void ContinueBuildingDocUsingInnerContent(DocXWriter doc, string xml_string)
        {
            bool skip_read = false;

            // I am sure this can be made more efficient unless GetBytes is actually
            // returning the underlying data and not producing a copy. I also do not
            // like how I am wrapping the <d> tag around it to make XmlReader parse
            // the input.
            var m = new MemoryStream(Encoding.UTF8.GetBytes(
                $"<d>{xml_string}</d>"
            ));

            var xml = XmlReader.Create(m);

            while (skip_read || xml.Read())
            {

                skip_read = false;

                switch (xml.NodeType)
                {
                    case XmlNodeType.Element:
                        switch (xml.Name)
                        {
                            case "endnotes-from-footnotes":
                                foreach (var pair in footnotes)
                                {
                                    doc.StartFootnoteCitation(
                                        (text, is_duplicate, refid) =>
                                        {
                                            if (footnote_to_refid.ContainsKey(text))
                                            {
                                                // Override the automatically generated and unique ID.
                                                refid = footnote_to_refid[text];
                                            }

                                            ContinueBuildingDocUsingInnerContent(doc, text);

                                            return refid;
                                        },
                                        pair.Value,
                                        only_output_definition: true
                                    );
                                }
                                break;
                            case "name":
                                doc.AddRunAndText(xml.ReadElementContentAsString(), StyleCreaterType.Name);
                                skip_read = true;
                                break;
                            case "phone":
                                doc.AddRunAndText(xml.ReadElementContentAsString(), StyleCreaterType.Phone);
                                skip_read = true;
                                break;
                            case "url":
                                {
                                    var inner_xml = xml.ReadInnerXml();
                                    doc.AddHyperlink(inner_xml);
                                    //doc.AddRunAndText(inner_xml, StyleCreaterType.Url);
                                    skip_read = true;
                                    break;
                                }
                            case "email":
                                doc.AddRunAndText(xml.ReadElementContentAsString(), StyleCreaterType.Email);
                                skip_read = true;
                                break;
                            case "keyword":
                                {
                                    var val = xml.ReadElementContentAsString();
                                    doc.AddRunAndText(val, StyleCreaterType.Keyword);
                                    skip_read = true;

                                    if (feature_keyword_auto_cite)
                                    {
                                        var key = $"{feature_keyword_auto_cite_prefix}_{val}".ToLower();

                                        skip_read = true;

                                        if (footnotes.ContainsKey(key))
                                        {
                                            doc.StartFootnoteCitation(
                                                (a, b, c) => {
                                                    return BuildFootNote(doc, a, b, c);
                                                },
                                                footnotes[key]
                                            );
                                        }
                                        break;
                                    }
                                    break;
                                }
                            case "strong":
                                doc.AddRunAndText(xml.ReadElementContentAsString(), StyleCreaterType.Strong);
                                skip_read = true;
                                break;
                            case "title":
                                doc.AddRunAndText(xml.ReadElementContentAsString(), StyleCreaterType.LongWork);
                                skip_read = true;
                                break;
                            case "c":
                                {
                                    var key = xml.ReadElementContentAsString();

                                    skip_read = true;

                                    if (footnotes.ContainsKey(key))
                                    {
                                        doc.StartFootnoteCitation(
                                            (a, b, c) => {
                                                return BuildFootNote(doc, a, b, c);
                                            },
                                            footnotes[key]
                                        );
                                    }
                                    else
                                    {
                                        throw new Exception($"Missing citation data for key {key}.");
                                    }
                                    break;
                                }
                        }
                        break;
                    case XmlNodeType.Comment:
                        break;

                    case XmlNodeType.Text:
                        doc.AddRunAndText(xml.Value);
                        break;

                    case XmlNodeType.XmlDeclaration:
                        break;

                    case XmlNodeType.Whitespace:
                        break;

                    case XmlNodeType.EndElement:
                        switch (xml.Name)
                        {
                            case "d":
                                break;
                            default:
                                throw new Exception("Unknown ending element.");
                        }
                        break;

                    default:
                        throw new Exception($"Unsupport XML node type as {xml.NodeType}.");
                }
            }
        }

        public void Run(Stream xml_input, string output_name)
        {
            var doc = new DocXWriter(output_name, this.style_creator);

            bool skip_read = false;

            int section_level = 0;

            var citations = new Dictionary<string, string>();

            var xml = XmlReader.Create(xml_input);

            while (skip_read || xml.Read())
            {

                skip_read = false;

                switch (xml.NodeType)
                {
                    case XmlNodeType.Element:
                        switch (xml.Name)
                        {
                            case "keyword-auto-cite":
                                var enabled = bool.Parse(xml.GetAttribute("enabled"));
                                var prefix = xml.GetAttribute("prefix");

                                feature_keyword_auto_cite = enabled;
                                feature_keyword_auto_cite_prefix = prefix;
                                break;

                            case "footnote":
                                var key = xml.GetAttribute("key");
                                var val = xml.ReadInnerXml();
                                footnotes.Add(key, val);
                                break;

                            case "d":
                                break;

                            case "s":
                                doc.AddSection(section_level, xml.GetAttribute("title"));
                                section_level++;
                                break;

                            case "p":
                                doc.AddParagraph();

                                var inner_xml = xml.ReadInnerXml();

                                ContinueBuildingDocUsingInnerContent(doc, inner_xml);

                                doc.GotoParent();
                                break;
                            default:
                                throw new Exception($"Unknown element tag {xml.Name}.");
                        }
                        break;
                    case XmlNodeType.EndElement:
                        switch (xml.Name)
                        {
                            case "d":
                                break;
                            case "s":
                                section_level--;
                                break;
                            case "p":
                                throw new Exception("Should not have reached </p> tag. It should have been consumed before now.");
                            default:
                                throw new Exception($"Unknown closing element tag {xml.Name}.");
                        }
                        break;

                    case XmlNodeType.Text:
                        throw new Exception("Text must be within a paragraph.");

                    case XmlNodeType.XmlDeclaration:
                        break;

                    case XmlNodeType.Whitespace:
                        break;

                    case XmlNodeType.Comment:
                        break;

                    default:
                        throw new Exception($"XML node type {xml.NodeType} is not implemented.");
                }
            }

            doc.Save();
        }
    }
}