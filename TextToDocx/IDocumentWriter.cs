﻿using System;
using DocumentFormat.OpenXml;

namespace TextToDocx
{
    interface IDocumentWriter
    {
        void AddHyperlink(string v, StyleCreaterType style_type = StyleCreaterType.ParagraphText, int level = 0);
        void AddParagraph(int level = 0);
        void AddRunAndText(string v, StyleCreaterType style_type = StyleCreaterType.ParagraphText, int level = 0);
        void AddSection(int level, string title);
        void Dispose();
        void GotoParent();
        void PopCurrentElement();
        void PushCurrentElement();
        void Save();
        void SetCurrentElement(OpenXmlElement current_element);
        void StartFootnoteCitation(Func<string, bool, int, int> builder, string text, int level = 0, bool only_output_definition = false);
    }
}